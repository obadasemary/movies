//
//  DetailsMovieViewController.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/3/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class DetailsMovieViewController: UIViewController {
    
    // MARK: - shared
    
    static var shared = DetailsMovieViewController()

    // MARK: - @IBOutlet
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var iMDBLabel: UILabel!
    @IBOutlet weak var summaryTextView: UITextView!
    
    @IBOutlet weak var favoriteView: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var unFavoriteView: UIImageView!
    @IBOutlet weak var unFavoriteButton: UIButton!
    // MARK: - init
    
    var detailsmovie: Movie?
    var favoriteMoviesId = [Int]()
    var favoriteMoviesKey = "FavoriteMovies"
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieImageView.sd_setImage(with: URL(string: (detailsmovie?.url)!), placeholderImage: UIImage(named: "placeholder.png"))
        
        titleLabel.text = detailsmovie?.name
        releaseDateLabel.text = "Release Date: \((detailsmovie?.relaeseDate)!)"
        rateLabel.text = "\((detailsmovie?.rate)!)"
        iMDBLabel.text = "IMDB Rate"
        summaryTextView.text = detailsmovie?.description
        
        if let storedFavoriteMoviesId = UserDefaults.standard.array(forKey: favoriteMoviesKey) {
            favoriteMoviesId = storedFavoriteMoviesId as! [Int]
        }
        
        if favoriteMoviesId.contains((detailsmovie?.id)!) {
            favoriteView.isHidden = true
            favoriteButton.isHidden = true
            unFavoriteView.isHidden = false
            unFavoriteButton.isHidden = false
        } else {
            favoriteView.isHidden = false
            favoriteButton.isHidden = false
            unFavoriteView.isHidden = true
            unFavoriteButton.isHidden = true
        }
    }
    
    // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = detailsmovie?.name
    }
    
    // MARK: - becomeFavorite
    
    @IBAction func becomeFavorite(_ sender: Any) {
        favoriteView.isHidden = true
        favoriteButton.isHidden = true
        unFavoriteView.isHidden = false
        unFavoriteButton.isHidden = false
        
        favoriteMoviesId.append((detailsmovie?.id)!)
        UserDefaults.standard.set(favoriteMoviesId, forKey: favoriteMoviesKey)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - becomeUnFavorite
    
    @IBAction func becomeUnFavorite(_ sender: Any) {
        favoriteView.isHidden = false
        favoriteButton.isHidden = false
        unFavoriteView.isHidden = true
        unFavoriteButton.isHidden = true
        
        favoriteMoviesId = favoriteMoviesId.filter({ $0 != (detailsmovie?.id)! })
        UserDefaults.standard.set(favoriteMoviesId, forKey: favoriteMoviesKey)
        UserDefaults.standard.synchronize()
    }
}
