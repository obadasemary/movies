//
//  MoviesViewController.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/1/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit
import ImageSlideshow
import NVActivityIndicatorView
import SDWebImage

class MoviesViewController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, FavoriteStatusChanges {
    
    // MARK: - shared
    
    static var shared = MoviesViewController()
    
    // MARK: - @IBOutlet
    
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - init
    
    let sizeNVActivityIndicator = CGSize(width: 30, height:30)
    
    var moviesList = [Movie]()
    var favoriteMoviesId = [Int]()
    var favoriteMoviesKey = "FavoriteMovies"

    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startAnimating(self.sizeNVActivityIndicator, message: "Loading...", type: NVActivityIndicatorType.ballClipRotateMultiple)
        
        if let storedFavoriteMoviesId = UserDefaults.standard.array(forKey: favoriteMoviesKey) {
            favoriteMoviesId = storedFavoriteMoviesId as! [Int]
        }
        
        let jsonUrlMoviesString = "http://misc.solidict.com/movies/"
        
        guard let urlMovies = URL(string: jsonUrlMoviesString) else { return }
        
        MoviesController.shared.getMovies(urlMovies: urlMovies) { (success, message, movies) in
            
            if success {
                
                DispatchQueue.main.async { // Correct
                    
                    var imagesData = [String]()
                    
                    for ban in (movies?.banner)! {
                        imagesData.append(ban.url)
                    }
                    
                    self.setSliderData()
                    
                    if imagesData.count > 0 {
                        let arr =  imagesData.map {
                            
                            SDWebImageSource(urlString: $0 )!
                        }
                        self.slideshow.setImageInputs(arr)
                    }
                    
                    self.moviesList = (movies?.movies)!
                }
                
                DispatchQueue.main.async {
                    
                    self.tableView.reloadData()
                    self.stopAnimating()
                }
            }
        }
    }
    
    // MARK: - viewWillDisappear
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.title = ""
    }
    
    //  MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.moviesList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 140
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MoviesTableViewCell
        
        if moviesList[indexPath.row].id == 2 || moviesList[indexPath.row].id == 4 {
            
            cell.newImageView.isHidden = false
        } else {
           cell.newImageView.isHidden = true
        }
        
        cell.titleLabel?.text = moviesList[indexPath.row].name
        cell.releaseDateLabel.text = "Release Date: \(moviesList[indexPath.row].relaeseDate)"
        
        cell.movieImageView.sd_setImage(with: URL(string: moviesList[indexPath.row].url), placeholderImage: UIImage(named: "placeholder.png"))
        
//        let downloader = SDWebImageDownloader.shared()
//        downloader?.downloadImage(with: NSURL(string: (moviesList[indexPath.row].url)) as URL!, options: [], progress: nil, completed: { (image, data, error, finished) in
//
//            DispatchQueue.main.async {
//
//                cell.movieImageView.image = image
//            }
//        })
        
        cell.delegate = self
        
        cell.movieId = moviesList[indexPath.row].id
        
        if favoriteMoviesId.contains(moviesList[indexPath.row].id) {
            cell.favoriteButton.isHidden = true
            cell.unFavoriteButton.isHidden = false
            cell.favoriteLable.text = "Favorilere eklendi"
            cell.favoriteLable.textColor = UIColor.rgb(red: 253, green: 176, blue: 9)
        } else {
            cell.favoriteButton.isHidden = false
            cell.unFavoriteButton.isHidden = true
            cell.favoriteLable.text = "Favorilere ekle"
            cell.favoriteLable.textColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "ShowMovieDetails", sender: self)
        
        DetailsMovieViewController.shared.detailsmovie = moviesList[indexPath.row]
    }
    
    // MARK: - prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowMovieDetails" {
            
            DetailsMovieViewController.shared = segue.destination as! DetailsMovieViewController
        }
    }
    
    // MARK: - onFavoriteChanged
    
    func onFavoriteChanged(isFavorite: Bool, movieId: Int) {
        
        if isFavorite {
            
            favoriteMoviesId.append(movieId)
            UserDefaults.standard.set(favoriteMoviesId, forKey: favoriteMoviesKey)
            UserDefaults.standard.synchronize()
            
        } else {
            
            favoriteMoviesId = favoriteMoviesId.filter({ $0 != movieId })
            UserDefaults.standard.set(favoriteMoviesId, forKey: favoriteMoviesKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    //  MARK: - Slider
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }

    func setSliderData() {
        slideshow.backgroundColor = UIColor.black
        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = PageControlPosition.underScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.rgb(red: 253, green: 176, blue: 9)
        slideshow.pageControl.pageIndicatorTintColor = UIColor.gray
        slideshow.contentScaleMode = UIViewContentMode.scaleToFill
        slideshow.zoomEnabled = false
        slideshow.currentPageChanged = { page in
//            print("current page:", page)
        }
    }
}
