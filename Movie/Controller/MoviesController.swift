//
//  MoviesController.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/2/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import Foundation

class MoviesController: NSObject {
    
    // MARK: - shared
    
    static let shared  = MoviesController()
    
    // MARK: - getMovies
    
    func getMovies(urlMovies: URL, completion: @escaping (_ success: Bool, _ message: String, _ moviesResponse: PurpleData?) -> Void ) {
        
        URLSession.shared.dataTask(with: urlMovies) { (data, response, error) in
            
            guard let data = data else { return }
            
            do {
                
                let movi = try JSONDecoder().decode(Movies.self, from: data)
            
                completion(movi.success, movi.message, movi.data)
                
            } catch let jsonError {
                
                completion(false, "Error", nil)
                print("Error serializing json:", jsonError)
            }
            
        }.resume()
    }
}
