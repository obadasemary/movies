//
//  MoviesTableViewCell.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/2/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit

protocol FavoriteStatusChanges {
    
    func onFavoriteChanged(isFavorite: Bool, movieId: Int)
}

class MoviesTableViewCell: UITableViewCell {
    
    // MARK: - @IBOutlet

    @IBOutlet weak var newImageView: UIImageView!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var favoriteLable: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var unFavoriteButton: UIButton!
    
    // MARK: - init
    
    var movieId: Int!
    
    var delegate: FavoriteStatusChanges!
    
    // MARK: - awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        favoriteButton.addTarget(self, action: #selector(becomeFavorite), for: .touchUpInside)
        unFavoriteButton.addTarget(self, action: #selector(unBecomeFavorite), for: .touchUpInside)
    }
    
    // MARK: - setSelected

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - becomeFavorite
    
    @objc func becomeFavorite() {
        favoriteButton.isHidden = true
        unFavoriteButton.isHidden = false
        favoriteLable.textColor = UIColor.rgb(red: 253, green: 176, blue: 9)
        favoriteLable.text = "Favorilere eklendi"
        
        delegate.onFavoriteChanged(isFavorite: true, movieId: movieId)
    }
    
    // MARK: - unBecomeFavorite
    
    @objc func unBecomeFavorite() {
        unFavoriteButton.isHidden = true
        favoriteButton.isHidden = false
        favoriteLable.textColor = UIColor.white
        favoriteLable.text = "Favorilere ekle"
        
        delegate.onFavoriteChanged(isFavorite: false, movieId: movieId)
    }
    
    // MARK: - prepareForReuse
    
    override func prepareForReuse() {
        
        movieImageView.image = UIImage()
    }
}
