//
//  Extensions.swift
//  Otube
//
//  Created by Abdelrahman Mohamed on 11/26/17.
//  Copyright © 2017 Abdelrahman Mohamed. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}
