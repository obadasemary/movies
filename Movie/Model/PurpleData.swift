//
//  PurpleData.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/2/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import Foundation

struct PurpleData: Decodable {
    let banner: [Banner]
    let movies: [Movie]
}
