//
//  Movie.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/2/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    let id: Int
    let name, description, url, relaeseDate: String
    let rate: Double
}
