//
//  Movies.swift
//  Movie
//
//  Created by Abdelrahman Mohamed on 1/2/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import Foundation

struct Movies: Decodable {
    let success: Bool
    let message: String
    let data: PurpleData
}
